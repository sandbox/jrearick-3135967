<?php

/**
 * @file
 * Contains the \Drupal\qualtrics_iframe\Plugin\Field\FieldFormatter\UrlQualtricsIframe.
 */

namespace Drupal\qualtrics_iframe\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'qualtrics_iframe' formatter.
 *
 * @FieldFormatter(
 *   id = "qualtrics_iframe",
 *   label = @Translation("Qualtrics Survery iframe"),
 *   field_types = {"link"},
 * )
 */
class UrlQualtricsIframe extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode)
  {
    $elements = [];
    foreach ($items as $delta => $item) {

      // $url = 'https://iastate.qualtrics.com/jfe/form/SV_56II0mdJXbMw2sR';
      $url = $item->getUrl();
      $title = $item->getValue()['title'];
      if (empty($title)) {
        $title = $this->t('Go to the form at @url', ['@url' => $url->toString()]);
      }

      // Validate the URL. We want https and the domain to be *.qualtrics.com.
      $useIframe = FALSE;
      if ($scheme = parse_url($url->toString(), PHP_URL_SCHEME) === 'https') {
        if (stripos(parse_url($url->toString(), PHP_URL_HOST), '.qualtrics.com') !== FALSE) {
          $useIframe = TRUE;
        }
      }

      // Generate a simple href form the URL.
      $link = '<a href="' . $url->toString() . '">' . $title . '</a>';

      if ($useIframe) {
        $elements['#attached'] = [
          'library' => ['qualtrics_iframe/qualtrics-iframe-libraries'],
        ];
        $elements[$delta] = [
          // To allow iframe, we must use an inline_template instead of #markup.
          '#type' => 'inline_template',
          '#template' => '<div class="qualtrics_iframe_container" width="100%" height="100%"><iframe frameborder="0" scrolling="no" height="1024" width="100%" class="qualtrics_iframe" src="{{ url }}"">{{ link }}</iframe></div>',
          '#context' => [
            'url' => $url,
            'link' => $link,
          ]
        ];
      }
      else {
        $elements[$delta] = [
          '#markup' => $link,
        ];
      }
    }
    return $elements;
  }
}
