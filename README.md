# qualtrics_iframe
This module adds a formatter to the Link field that will display the URL in a responsive iframe.

## Usage
Add a _Link_ field to a content type or entity. Then, in _Manage display_ select _Qualtrics Survey iframe_ from the format drop-down. Blank out the _Trim link text length_ so URLs don't get cut off.

The formatter expects the link to be https and be from a qualtrics.com domain. If the URL is not, it will display a regular link.

## Dynamic Height iFrame
To enable dynamic-height iFrames, the framed page needs to be able to communicate back to our site what its height it, so we can resize it dynamically. To do so, we need to add this script to framed HTML.

```js
<script>
  Qualtrics.SurveyEngine.addOnload(function()
  {
    // Wait half a second and then adjust the iframe height to fit the questions
    setTimeout(function () {
      parent.postMessage( document.getElementById("SurveyEngineBody").scrollHeight + "px", "http://jrearick.vpn.iastate.edu");
    },
    500);
   });
</script>
```

Note, for security reasons, we need to define the protocol and domain of the site we are going to send the message to. In the example below it is `http://jrearick.vpn.iastate.edu/`. Change this to the URL of the root site the iFrame is going to be placed.

To add this code to a qualtrics survey

* Open Your Survey for editing
* Click on "Look & Feel"
* Click on the "General" tab
* On the "Header" field click on the "edit" link
* Click on the "Source" button on the editor toolbar
* Paste in the code
* Click Save
* Click Save
* Click "Publish" to publish the changes
